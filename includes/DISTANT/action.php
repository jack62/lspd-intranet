<?php

switch($_GET['action']){

    case 'effectif':

        $effectif = $bdd->prepare('SELECT * FROM users');
        $effectif->execute();
        
        echo '
        <a href="?action=addUser"><button style="width:100%;" class="btn btn-success">Ajouter un Agent</button></a>
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr class="headTable">
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Matricule</th>
            <th>Etat de l\'agent </th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>';
        while($donnees = $effectif->fetch()){
           
        $rank =  returnAction($_GET['action'],$donnees['id'],$_SESSION['matricule'],$bdd);
        $service = checkStatus($donnees['matricule'],$bdd);
            echo ' 
            <tr class="bodyTable">
            <td>'.$donnees['nom'].'</td>
            <td>'.$donnees['prenom'].'</td>
            <td>'.$donnees['matricule'].'</td>
            <td>'; 
                if($service == true){
                  echo '<span class="badge badge-success">En Service</span>';
                }
                else{
                  echo '<span class="badge badge-danger">Hors Service</span>';
                }
              
            echo '</td><td>'. $rank . '</td>
        </tr>';



        }
        echo '</tbody></table>';
    break;

    case 'delUser':
    $user = getUser($_SESSION['matricule'],$bdd);
        if($user['rank'] >= 999)
        {
            $del = $bdd->prepare('DELETE FROM users WHERE id = :id');
            $del->execute(array('id' => htmlspecialchars($_GET['id'])));
            header('location: intranet.php?action=effectif');
        }

    case 'edituser':
        $user = getUser($_SESSION['matricule'],$bdd);
        if($user['rank'] >= 4)
        {
        $id = htmlspecialchars($_GET['id']);
        
        $edit = $bdd->prepare('SELECT * FROM users WHERE id = :id');
        $edit->execute(array('id' => $id));
        $result = $edit->fetch();
        ?>
         <div class="editForm">
        <form action="?action=edituser&id=<?php echo $id; ?>&form=true" method="post" class="col-lg-12">
            <div class="form-group">
                <label for="">Nom </label>
                <input type="nom" class="form-control" name="nom" value="<?php echo $result['nom']; ?>">
            </div>

            <div class="form-group">
                <label for="">Prenom </label>
                <input type="prenom" class="form-control" name="prenom"  value="<?php echo $result['prenom']; ?>">
            </div>

            <div class="form-group">
                <label for="">Matricule </label>
                <input type="prenom" class="form-control" name="matricule"  value="<?php echo $result['matricule']; ?>">
            </div>

            <button type="submit" class="btn btn-info">VALIDER</button>

        </form>
            <?php  
                if(isset($_GET['form'])){
                    if($_GET['form'] == true){
                        $update = $bdd->prepare('UPDATE users SET nom = :nom, prenom = :prenom, matricule =  :matricule WHERE id = :id');
                        $update->execute(array(
                            'nom' => htmlspecialchars($_POST['nom']),
                            'prenom' => htmlspecialchars($_POST['prenom']),
                            'matricule' => htmlspecialchars($_POST['matricule']), 
                            'id' => htmlspecialchars($_GET['id'])
                        ));
                        echo '<div class="alert alert-success" role="alert">
                        L\'utilisateur est bien mise à jour !!!
                      </div>';
                    }
                }
            ?>
        </div>

        <?php
      
        }
        else{
            echo '<div class="alert alert-danger" role="alert">
            Cette fonction ne vous est pas permise !!!
          </div>';
          $message = 'Attention une personne tente d\'acceder à des fonctions privée '.$_SERVER['REMOTE_ADDR'].' Matricule '.$_SESSION['matricule'] ;
          $url = "https://discordapp.com/api/webhooks/607179655862026260/WZlQmf7pWT0RuXaQY1Oc7HnMbEB35JXVzD0mYJvPKi1GDh-UEG7Esev65jBITCkPVZk3";
          echo postToDiscord($message,$url);
        }

    break;

    case 'addUser':
        $user = getUser($_SESSION['matricule'],$bdd);
        if($user['rank'] >= 8)
        {
        ?>
                <form action="?action=addUser&Form=true" method="post" class="col-lg-12">
                    <div class="form-group">
                        <label for="">Nom </label>
                        <input type="text" class="form-control" name="nom">
                    </div>

                    <div class="form-group">
                        <label for="">Prenom </label>
                        <input type="text" class="form-control" name="prenom">
                    </div>

                    <div class="form-group">
                        <label for="">Matricule </label>
                        <input type="text" class="form-control" name="matricule">
                    </div>

                    <div class="form-group">
                        <label for="">Mot de Passe</label>
                        <input type="password" class="form-control" name="password">
                    </div>

                    <div class="form-group">
                        <label for="">Rang </label>
                        <select name="rank" class="form-control">
                                    <option value="0"> Cadet</option>
                                    <option value="1"> Brigadier</option>
                                    <option value="2"> Officier</option>
                                    <option value="3"> Sergent</option>
                                    <option value="4"> Sergent Chef</option>
                                    <option value="5"> Adjudant</option>
                                    <option value="6"> Major</option>
                                    <option value="7"> Lieutenant</option>
                                    <option value="8"> Capitaine</option>
                                    ';
                            
                            <?php 
                            if($user['rank'] >= 998){echo '
                                <option value="9"> Commandant</option>
                            <option value="999"> Développeur / Staff</option>';
                            }
                            echo $user['rank'];
                            ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-info">Ajouter</button>

                </form>
            <?php
            if(isset($_GET['Form'])){
                if($_GET['Form'] == true){
                    $password = password_hash($_POST['password'], PASSWORD_DEFAULT); 
                    $add = $bdd->prepare('INSERT INTO users(nom,prenom,matricule,password,rank) VALUES(:nom,:prenom,:matricule,:password,:rank)');
                    $add->execute(array(
                        'nom' => htmlspecialchars($_POST['nom']),
                        'prenom' => htmlspecialchars($_POST['prenom']),
                        'matricule' => htmlspecialchars($_POST['matricule']),
                        'password' => $password,
                        'rank' => htmlspecialchars($_POST['rank'])
                    ));
                    echo '<div class="alert alert-success" role="alert">
                    L\'utilisateur est bien ajouter !!!
                  </div>';
                  
                }
            }
        }
    break;

    case 'service':

    $message = 'Prise de service de '.$_SESSION['matricule'];
    $url = "https://discordapp.com/api/webhooks/609139856282746941/6b8TOWomhRqun3QdkG5MXZxL0d3ls5CKPI1ryy4TmSOjoPylJXne3djVIJyWhUcHAElS";
    echo postToDiscord($message,$url);
    
    $update = $bdd->prepare('UPDATE users SET statusAgent=1 WHERE matricule="'.$_SESSION['matricule'].'"');
    $update->execute();
    // echo serviceUpdate($_SESSION['id'],true,$bdd);
     header('location: intranet.php?action=effectif');
    break;

    case 'serviceOff':
        
            $message = 'Fin de service de '.$_SESSION['matricule'];
            $url = "https://discordapp.com/api/webhooks/609139856282746941/6b8TOWomhRqun3QdkG5MXZxL0d3ls5CKPI1ryy4TmSOjoPylJXne3djVIJyWhUcHAElS";
            echo postToDiscord($message,$url);
            $update = $bdd->prepare('UPDATE users SET statusAgent=0  WHERE matricule="'.$_SESSION['matricule'].'"');
            $update->execute();
            header('location: intranet.php');
    break;
        

    case 'casier':
    $casier = $bdd->prepare('SELECT * FROM casier');
    $casier->execute();
    ?>
        <a href="?action=casierAdd"><button style="width: 100%; border-radius: 0 !important;" class="btn btn-warning">Ajouter un casier !!</button></a>
        <table class="table table-striped">
        <thead class="thead-dark">
            <tr class="headTable">
                <th>Nom</th>
                <th>Prenom</th>
                <th>date de Naissance</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
        while($donnees = $casier->fetch())
        { 
           

            
            ?>
                <tr class="bodyTable">
                <td><?php echo $donnees['nom']; ?></td>
                <td><?php echo $donnees['prenom']; ?></td>
                <td><?php echo $donnees['ddn']; ?></td>
                <td><?php echo returnAction($_GET['action'],$donnees['id'],$_SESSION['matricule'],$bdd) ; ?></td>
                </tr>
            <?php
            
        }
            ?>
        </tbody>


        </table>

    <?php
    break;

    case 'casierView':
        $select = $bdd->prepare('SELECT * FROM casier WHERE id = :id ');
        $select->execute(array('id' => $_GET['id']));
        $result = $select->fetch();

        ?>
            <div class="card carddelit">
                <div class="card-header">
                    <?php echo $result['nom'].' '.$result['prenom'].' Née le '.$result['ddn']; ?>
                </div>
                <div class="card-body ">
                    <blockquote class="blockquote mb-0">
                    <p>Casier judiciaire concernant la personne de <?php echo $result['nom'].' '.$result['prenom']; ?> , don les fait sont cité ci-dessous</p>
                    <footer class="blockquote-footer"><?php echo $result['delit']; ?></footer>
                    </blockquote>
                </div>
            </div>


        <?php
    break;
    case 'casierEdit':

        $view = $bdd->prepare('SELECT * FROM casier WHERE id = :id');
        $view->execute(array('id' => htmlspecialchars($_GET['id'])));
        $result = $view->fetch();

        echo '
        <form action="?action=casierEdit&Form=true&id='.$result['id'].'" method="post" class="col-lg-12">
        <div class="form-group">
            <label for="">Nom </label>
            <input type="text" class="form-control" name="nom" value="'.htmlspecialchars($result['nom']).'">
        </div>

        <div class="form-group">
            <label for="">Prénom </label>
            <input type="text" class="form-control" name="prenom" value="'.htmlspecialchars($result['prenom']).'">
        </div>

        <div class="form-group">
            <label for="">Date de Naissance </label>
            <input type="text" class="form-control" name="ddn" value="'.htmlspecialchars($result['ddn']).'">
        </div>

        <div class="form-group">
            <label for="">Délit </label>
            <textarea class="form-control" id="editor1" rows="10" cols="80" name="delit" >'.htmlspecialchars($result['delit']).'</textarea>
        </div>

        <button type="submit" style="width:100%;" class="btn btn-success">Mettre à jour </button>
        </form>
        ';
        
        if(isset($_GET['Form'])){
            if($_GET['Form'] == 'true'){
                $update = $bdd->prepare('UPDATE casier SET nom= :nom, prenom= :prenom, ddn= :ddn, delit= :delit WHERE id = :id');
                $update->execute(array(
                    'nom' => $_POST['nom'],
                    'prenom' => $_POST['prenom'],
                    'ddn' => $_POST['ddn'],
                    'delit' => $_POST['delit'],
                    'id' => $_GET['id']
                ));
                echo '<div class="alert alert-success" role="alert">
                Le casier est bien modifier
              </div>';
              header('location: ?action=casier');
              
            }
        }


    break;

    case 'casierDelete':
    $user = getUser($_SESSION['matricule'],$bdd);
    if($user['rank'] >= 4)
    {
        $delete = $bdd->prepare('DELETE FROM casier WHERE id= :id');
        $delete->execute(array('id' => $_GET['id']));
        header('location: ?action=casier');
    }
    break;

    case 'casierAdd':
    ?>
     <form action="?action=casierAdd&Form=true" method="post" class="col-lg-12">
        <div class="form-group">
            <label for="">Nom </label>
            <input type="text" class="form-control" name="nom" >
        </div>

        <div class="form-group">
            <label for="">Prénom </label>
            <input type="text" class="form-control" name="prenom">
        </div>

        <div class="form-group">
            <label for="">Date de Naissance </label>
            <input type="text" class="form-control" name="ddn">
        </div>

        <div class="form-group">
            <label for="">Délit </label>
            <textarea class="form-control" id="editor1" rows="10" cols="80" name="delit" ></textarea>
        </div>

        <button type="submit" style="width:100%;" class="btn btn-success">Créer le casier </button>
        </form>
        <?php

        if(isset($_GET['Form'])){
            if($_GET['Form'] == true){
                $add = $bdd->prepare('INSERT INTO casier(nom,prenom,ddn,delit) VALUES(:nom,:prenom,:ddn,:delit)');
                $add->execute(array(
                    'nom' => $_POST['nom'],
                    'prenom' => $_POST['prenom'],
                    'ddn' => $_POST['ddn'],
                    'delit' => $_POST['delit']
                ));
                echo '<div class="alert alert-success" role="alert">
                Le casier est bien Créer
              </div>';
            }
        }
        break;

        /********************************************** */

        case 'enquete':
    $casier = $bdd->prepare('SELECT * FROM enquete');
    $casier->execute();
    ?>
        <a href="?action=enqAdd"><button style="width: 100%; border-radius: 0 !important;" class="btn btn-warning">Ajouter un casier !!</button></a>
        <table class="table table-striped">
        <thead class="thead-dark">
            <tr class="headTable">
                <th>Intitulé de l'enquête</th>
                <th>Nom du désigner</th>
                <th>Nom de l'agent</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
        while($donnees = $casier->fetch())
        { 
           

            
            ?>
                <tr class="bodyTable">
                <td><?php echo $donnees['title']; ?></td>
                <td><?php echo $donnees['contre']; ?></td>
                <td><?php echo $donnees['agent']; ?></td>
                <td><?php echo $donnees['date']; ?></td>
                <td><?php echo returnAction($_GET['action'],$donnees['id'],$_SESSION['matricule'],$bdd) ; ?></td>
                </tr>
            <?php
            
        }
            ?>
        </tbody>


        </table>

    <?php
    break;

    case 'enqView':
        $select = $bdd->prepare('SELECT * FROM enquete WHERE id = :id ');
        $select->execute(array('id' => $_GET['id']));
        $result = $select->fetch();

        ?>
            <div class="card carddelit">
                <div class="card-header">
                    <?php echo $result['title']; echo 'Enquête sur : '.$result['contre'];?>
                </div>
                <div class="card-body ">
                    <blockquote class="blockquote mb-0">
                    <p><?php echo $result['contenue']; ?></p>
                    <footer class="blockquote-footer">en date du : <?php echo $result['date']; ?> Par l'agent : <?php echo $result['agent']; ?></footer>
                    </blockquote>
                </div>
            </div>


        <?php
    break;
    case 'enqEdit':

        $view = $bdd->prepare('SELECT * FROM enquete WHERE id = :id');
        $view->execute(array('id' => $_GET['id']));
        $result = $view->fetch();

        echo '
        <form action="?action=enqEdit&Form=true&id='.$result['id'].'" method="post" class="col-lg-12">
        <div class="form-group">
            <label for="">Intitulé du dossier </label>
            <input type="text" class="form-control" name="title" value="'.htmlspecialchars($result['title']).'">
        </div>

        <div class="form-group">
            <label for="">Personnne concerné </label>
            <input type="text" class="form-control" name="contre" value="'.htmlspecialchars($result['contre']).'">
        </div>

        <div class="form-group">
            <label for="">Nom de L\'agent </label>
            <input type="text" class="form-control" name="agent" value="'.htmlspecialchars($result['agent']).'">
        </div>

        <div class="form-group">
            <label for="">Date de saisie </label>
            <input type="date" class="form-control" name="date" value="'.htmlspecialchars($result['date']).'">
        </div>

        <div class="form-group">
            <label for="">Contenue du dossier </label>
            <textarea class="form-control" id="editor1" rows="10" cols="80" name="contenue" >'.htmlspecialchars($result['contenue']).'</textarea>
        </div>

        <button type="submit" style="width:100%;" class="btn btn-success">Mettre à jour </button>
        </form>
        ';
        
        if(isset($_GET['Form'])){
            if($_GET['Form'] == 'true'){
                $update = $bdd->prepare('UPDATE enquete SET title= :title, contre= :contre, agent= :agent, date= :date, contenue= :contenue WHERE id = :id');
                $update->execute(array(
                    'title' => $_POST['title'],
                    'contre' => $_POST['contre'],
                    'agent' => $_POST['agent'],
                    'date' => $_POST['date'],
                    'contenue' => $_POST['contenue'],
                    'id' => $_GET['id']
                ));
                echo '<div class="alert alert-success" role="alert">
                Le dossier d\'enquête est bien modifier
              </div>';
              header('location: ?action=enquete');
              
            }
        }


    break;

    case 'enqDelete':
    $user = getUser($_SESSION['matricule'],$bdd);
    if($user['rank'] >= 4)
    {
        $delete = $bdd->prepare('DELETE FROM enquete WHERE id= :id');
        $delete->execute(array('id' => $_GET['id']));
        header('location: ?action=enquete');
    }
    break;

    case 'enqAdd':
    ?>
     <form action="?action=enqAdd&Form=true" method="post" class="col-lg-12">
        <div class="form-group">
            <label for="">Intitulé de votre enquête </label>
            <input type="text" class="form-control" name="title" >
        </div>

        <div class="form-group">
            <label for=""> Nom de la personne contre qui vous enquêter</label>
            <input type="text" class="form-control" name="contre">
        </div>

        <div class="form-group">
            <label for="">Nom de l'agent en charge </label>
            <input type="text" class="form-control" name="agent">
        </div>

        <div class="form-group">
            <label for="">Date de saisie </label>
            <input type="date" class="form-control" name="date">
        </div>

        <div class="form-group">
            <label for="">Contenu de votre enquête  </label>
            <textarea class="form-control" id="editor1" rows="10" cols="80" name="contenue" ></textarea>
        </div>

        <button type="submit" style="width:100%;" class="btn btn-success">Créer le dossier</button>
        </form>
        <?php

        if(isset($_GET['Form'])){
            if($_GET['Form'] == true){
                $add = $bdd->prepare('INSERT INTO enquete(title,contre,agent,date,contenue) VALUES(:title,:contre,:agent,:date,:contenue)');
                $add->execute(array(
                    'title' => $_POST['title'],
                    'contre' => $_POST['contre'],
                    'agent' => $_POST['agent'],
                    'date' => $_POST['date'],
                    'contenue' => $_POST['contenue'],
                ));
                echo '<div class="alert alert-success" role="alert">
                Le dossier d\'enquête est bien Créer
              </div>';
            }
        }
        break;


                /********************************************** */

                case 'rapport':
                $casier = $bdd->prepare('SELECT * FROM rapport');
                $casier->execute();
                ?>
                    <a href="?action=rapAdd"><button style="width: 100%; border-radius: 0 !important;" class="btn btn-warning">Ajouter rapport !!</button></a>
                    <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr class="headTable">
                            <th>rapport établie par </th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                    while($donnees = $casier->fetch())
                    { 
                       
            
                        
                        ?>
                            <tr class="bodyTable">
                            <td><?php echo $donnees['agent']; ?></td>
                            <td><?php echo $donnees['date']; ?></td>
                            <td><?php echo returnAction($_GET['action'],$donnees['id'],$_SESSION['matricule'],$bdd) ; ?></td>
                            </tr>
                        <?php
                        
                    }
                        ?>
                    </tbody>
            
            
                    </table>
            
                <?php
                break;
            
                case 'rapView':
                    $select = $bdd->prepare('SELECT * FROM rapport WHERE id = :id ');
                    $select->execute(array('id' => $_GET['id']));
                    $result = $select->fetch();
            
                    ?>
                        <div class="card carddelit">
                            <div class="card-header">
                               Le :  <?php echo $result['date']; echo  "Rapport d'intervention de :".$result['agent'];?>
                            </div>
                            <div class="card-body ">
                                <blockquote class="blockquote mb-0">
                                <p><?php echo $result['contenu']; ?></p>
                                </blockquote>
                            </div>
                        </div>
            
            
                    <?php
                break;
                case 'rapEdit':
                    $user = getUser($_SESSION['matricule'],$bdd);
                    if($user['rank'] >= 4)
                    {
                        $view = $bdd->prepare('SELECT * FROM rapport WHERE id = :id');
                        $view->execute(array('id' => $_GET['id']));
                        $result = $view->fetch();
                
                        echo '
                        <form action="?action=rapEdit&Form=true&id='.$result['id'].'" method="post" class="col-lg-12">
                        <div class="form-group">
                            <label for="">Agent rédacteur </label>
                            <input type="text" class="form-control" name="agent" value="'.htmlspecialchars($result['agent']).'">
                        </div>
                
                        <div class="form-group">
                            <label for="">Date du rapport </label>
                            <input type="text" class="form-control" name="date" value="'.htmlspecialchars($result['date']).'">
                        </div>

                        <div class="form-group">
                            <label for="">Contenue du Rapport </label>
                            <textarea class="form-control" id="editor1" rows="10" cols="80" name="contenu" >'.htmlspecialchars($result['contenu']).'</textarea>
                        </div>
                
                        <button type="submit" style="width:100%;" class="btn btn-success">Mettre à jour </button>
                        </form>
                        ';
                    }
                    
                    if(isset($_GET['Form'])){
                        if($_GET['Form'] == 'true'){
                            $update = $bdd->prepare('UPDATE rapport SET agent= :agent, date= :date, contenu= :contenu WHERE id = :id');
                            $update->execute(array(
                                'agent' => $_POST['agent'],
                                'date' => $_POST['date'],
                                'contenu' => $_POST['contenu'],
                                'id' => $_GET['id']
                            ));
                            echo '<div class="alert alert-success" role="alert">
                            Le rapport est bien modifier
                          </div>';
                          header('location: ?action=rapport');
                          
                        }
                    }
            
            
                break;
            
                case 'rapDelete':
                $user = getUser($_SESSION['matricule'],$bdd);
                if($user['rank'] >= 4)
                {
                    $delete = $bdd->prepare('DELETE FROM rapport WHERE id= :id');
                    $delete->execute(array('id' => $_GET['id']));
                    header('location: ?action=rapport');
                }
                break;
            
                case 'rapAdd':
                ?>
                 <form action="?action=rapAdd&Form=true" method="post" class="col-lg-12">
                        <div class="form-group">
                            <label for="">Agent rédacteur </label>
                            <input type="text" class="form-control" name="agent">
                        </div>
                
                        <div class="form-group">
                            <label for="">Date du rapport </label>
                            <input type="date" class="form-control" name="date">
                        </div>

                        <div class="form-group">
                            <label for="">Contenue du Rapport </label>
                            <textarea class="form-control" id="editor1" rows="10" cols="80" name="contenu" ></textarea>
                        </div>
                
                        <button type="submit" style="width:100%;" class="btn btn-success">Créer le rapport </button>
                        </form>
                    <?php
            
                    if(isset($_GET['Form'])){
                        if($_GET['Form'] == true){
                            $add = $bdd->prepare('INSERT INTO rapport(agent,date,contenu) VALUES(:agent,:date,:contenu)');
                            $add->execute(array(
                                'agent' => $_POST['agent'],
                                'date' => $_POST['date'],
                                'contenu' => $_POST['contenu']
                            ));
                            echo '<div class="alert alert-success" role="alert">
                            Le Rapport est bien Créer
                          </div>';
                        }
                    }
                    break;
    






}

?>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>