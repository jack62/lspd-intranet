<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="#"><img src="images/logo.png" class="logonav" > </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="intranet.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=effectif">Effectif de Police</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action police
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item onservice" href="?action=service">Prise de service</a>
          <a class="dropdown-item offservice" href="?action=serviceOff">Fin de Service</a>
          <a class="dropdown-item" href="?action=casier">Liste des Casier Judiciaire</a>
          <a class="dropdown-item" href="?action=enquete">Liste des enquêtes</a>
          <a class="dropdown-item" href="?action=rapport">Liste des Rapports</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <div class="profilogin my-2 my-sm-0">
    Effectif en service <span class="badge badge-dark"><?php echo getService($bdd); ?></span>
    </div>
      <div class="profilogin my-2 my-sm-0"><?php echo 'Bienvenue'.$myFunc['prenom'].' '.$myFunc['nom'];?> <a href="logout.php"><img src="https://icon-library.net/images/log-off-icon/log-off-icon-23.jpg" style="max-width: 20px; max-height:20px;"></a>
      <?php
      $service = checkStatus($_SESSION['matricule'],$bdd);
     
        if($service == true){
          echo '<span class="badge badge-success">En Service</span>';
        }
        else{
          echo '<span class="badge badge-danger">Hors Service</span>';
        }
      
      ?>
      </div>
    </form>
  </div>
</nav>



