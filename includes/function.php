<?php


function getUser($user,$bdd){
    $select = $bdd->prepare('SELECT * FROM users WHERE matricule="'.$user.'"');
    $select->execute();

    $result = $select->fetch();

    $array = array(
    'nom' => $result['nom'],
    'prenom' => $result['prenom'],
    'rank' => $result['rank'],
    'matricule' => $result['matricule']
    );

    return $array;
}

function returnAction($page,$id,$users,$bdd){
    $user = $bdd->prepare('SELECT * FROM users WHERE matricule= :matricule');
    $user->execute(array('matricule' => $users));
    $return = $user->fetch();
    if($page == 'effectif')
    {
        switch($return['rank']){
            case '0': // cadet 
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            $action = '<span class="badge badge-danger">Non permis</span>';
            return $action;
            break;

            
            case '8': // Capitaine
            case '9': // Commandant 
            case '999': // Développeur
            $action = '<a href="?action=edituser&id='.$id.'"><button type="button" class="btn btn-info">Editer</button></a><a href="?action=delUser&id='.$id.'"><button type="button" class="btn btn-danger">Supprimer</button></a>';
            return $action;
            break;
        }
    }
    else if($page == 'casier')
    {
        switch($return['rank']){
            case '0': // cadet 
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            $action = '<a href="?action=casierView&id='.$id.'"><button class="btn btn-info"><i class="fa fa-eye"></i></button></a> <a href="?action=casierEdit&id='.$id.'"><button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button></a>';
            return $action;
            break;

            
            case '8': // Capitaine
            case '9': // Commandant 
            case '999': // Développeur
            $action = '<a href="?action=casierView&id='.$id.'"><button class="btn btn-info"><i class="fa fa-eye"></i></button></a> <a href="?action=casierEdit&id='.$id.'"><button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button></a>  <a href="?action=casierDelete&id='.$id.'"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>';
            return $action;
            break;
        }
    }
    else if($page == 'enquete')
    {
        switch($return['rank']){
            case '0': // cadet 
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            $action = '<a href=?action=enqView&id='.$id.'"><button class="btn btn-info">Voir</button></a> <a href="?action=enqEdit&id='.$id.'"><button type="button" class="btn btn-warning">Editer</button></a>';
            return $action;
            break;

            
            case '8': // Capitaine
            case '9': // Commandant 
            case '999': // Développeur
            $action = '<a href="?action=enqView&id='.$id.'"><button class="btn btn-info">Voir</button></a> <a href="?action=enqEdit&id='.$id.'"><button type="button" class="btn btn-warning">Editer</button></a>  <a href="?action=enqDelete&id='.$id.'"><button type="button" class="btn btn-danger">Supprimer</button></a>';
            return $action;
            break;
        }
    }

    else if($page == 'rapport')
    {
        switch($return['rank']){
            case '0': // cadet 
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            $action = '<a href=?action=rapView&id='.$id.'"><button class="btn btn-info">Voir</button></a> <a href="?action=rapEdit&id='.$id.'"><button type="button" class="btn btn-warning">Editer</button></a>';
            return $action;
            break;

            case '8': // Capitaine
            case '9': // Commandant 
            case '999': // Développeur
            $action = '<a href="?action=rapView&id='.$id.'"><button class="btn btn-info">Voir</button></a> <a href="?action=rapEdit&id='.$id.'"><button type="button" class="btn btn-warning">Editer</button></a>  <a href="?action=rapDelete&id='.$id.'"><button type="button" class="btn btn-danger">Supprimer</button></a>';
            return $action;
            break;
        }
    }
}

// function send discord
function postToDiscord($message,$url)
{
    $data = array("content" => $message, "username" => "Central Police");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    return curl_exec($curl);
}


function serviceUpdate($id,$status,$bdd){
    $check = $bdd->prepare('UPDATE users SET statusAgent = :status WHERE matricule= :matricule');
    $check->execute(array(
        'status' => $status,
        'matricule' => $id
    ));
    
}

function checkStatus($id,$bdd){
    $select = $bdd->prepare('SELECT * FROM users WHERE matricule = :id');
    $select->execute(array(
        'id' => $id
    ));
    $return = $select->fetch();
    return $return['statusAgent'];
}

function getService($bdd){
    $service = $bdd->prepare('SELECT COUNT(*) AS nbr FROM users WHERE statusAgent = 1');
    $service->execute();

    $result = $service->fetch();

    return $result['nbr'];
}