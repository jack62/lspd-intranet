<?php
require_once('includes/top.php');
?>
<body class="intranet">

<?php 
require_once('includes/config.php');
require_once('includes/function.php');
$myFunc = getUser($_SESSION['matricule'],$bdd);
require_once('includes/menu.php'); 
echo '<div class="container">';
echo $_SESSION['id'];

if(isset($_GET['action'])){
    require_once('includes/action.php');
}
else{
    $news  = $bdd->prepare('SELECT * FROM news ORDER BY id ASC LIMIT 3');
    $news->execute();

    while($donnees = $news->fetch()){
    ?>
    
        <div class="card cardnews col-lg-4">
            <div class="card-header"><?php echo $donnees['title'];  ?></div>
            <div class="card-body"><?php echo $donnees['contenue'];  ?></div> 
            <div class="card-footer"> <div class="profilogin cardprofil my-2 my-sm-0">Le : <?php echo $donnees['date'];  ?></div>  <div class="profilogin cardprofil my-2 my-sm-0"> Par : <?php echo $donnees['author'];  ?></div></div>
        </div>
    
<?php 
    }
}
?>
</div>
</body>
</html>