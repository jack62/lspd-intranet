<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Los Santos Police Dep Intranet</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
<body class="home">
<?php session_start();
if(isset($_SESSION['id']))
{

    header('location: intranet.php');

}
else{

?>
    <div class="container-fluid">
    <?php
    if(isset($_GET['erreur'])){
        if($_GET['erreur'] ==  true){
            echo '<div class="alert alert-danger" role="alert">
           '.$_GET['message'].'
          </div>';
        }
    }

     ?>
                <h2>LOS SANTOS POLICE DEPARTMENT</h2>
                <div class="loginForm">
                    <img src="images/logo.png" class="logo">
                    <form action="login.php" method="post">
                            
                            <div class="form-group">
                                    <label for="matricule">Matricule:</label>
                                    <input type="text" class="form-control" name="matricule" id="matricule">
                            </div>

                            <div class="form-group">
                                    <label for="password">Mot de passe :</label>
                                    <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <button class="btn btn-perso" type="submit">Connexion</button>

                    </form>
        </div>
    </div>
<?php 
}
?>    
</body>
</html>