<?php
require_once('includes/config.php');

if(!isset($_SESSION['login'])){
    

    //  Récupération de l'utilisateur et de son pass hashé
    $matricule = htmlspecialchars($_POST['matricule']);


    $req = $bdd->prepare('SELECT matricule, password FROM users WHERE matricule = :matricule');
    $req->execute(array(
        'matricule' => $matricule));
    $resultat = $req->fetch();

    // Comparaison du pass envoyé via le formulaire avec la base


    if ($resultat['matricule'] == $matricule && password_verify($_POST['password'], $resultat['password']))
    {
      
            $_SESSION['id'] = $resultat['id'];
            $_SESSION['matricule'] = $resultat['matricule'];
            echo 'Vous êtes connecté !';
            header('location: intranet.php');
         
       
    }
    else
    {
        $erreur =  'Mauvais identifiant ou mot de passe !';
        header('location: index.php?erreur=true&message='.$erreur.'');
       
    }



}
else{
     header('location: index.php');
}








